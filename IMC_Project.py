import csv
import re
with open("dataset.csv", "r") as data:
    leitor = csv.DictReader(data, delimiter=";")
    for linha in leitor:
        if '' not in (linha["Peso (kg)"], linha["Altura (m)"]):
            nome = linha['Primeiro Nome'].strip() + " " + re.sub("\s+", " ", linha['Sobrenomes']).strip()
            peso = float(linha["Peso (kg)"].replace(',', '.'))
            altura = float(linha["Altura (m)"].replace(',', '.'))
            calculoIMC = peso / (altura * altura)
            resultado = (f'{nome.upper()} {calculoIMC:.2f}'.replace('.', ','))
            print(resultado)
